# Copyright (c) 2019-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://www.vagrantup.com/docs/vagrantfile/vagrant_version.html
Vagrant.require_version '>= 2.0.0'

# https://www.vagrantup.com/docs/vagrantfile/version.html
Vagrant.configure(2) do |vagrant|

  # https://www.vagrantup.com/docs/provisioning/ansible_common.html#compatibility_mode
  ansible_compatibility_mode = '2.0'

  # https://www.vagrantup.com/docs/provisioning/ansible_common.html#extra_vars
  ansible_extra_vars = {
    'docker_host_group_members' => ['vagrant' => nil],
  }

  # https://www.vagrantup.com/docs/provisioning/ansible.html
  ansible_playbook = File.join(__dir__, 'provision-docker-hosts.yml')

  # https://ruby-doc.org/stdlib/libdoc/yaml/rdoc/YAML.html#method-c-load_file
  # https://docs.gitlab.com/ee/ci/yaml/#image
  YAML.load_file(File.join(__dir__, '.gitlab-ci.yml')).tap do |image_source|
    image_mapper = ->(_, item) { {'name' => item['image']} }
    image_selector = ->(_, item) { item.is_a?(Hash) && item.key?('image') }
    images = image_source.select(&image_selector).map(&image_mapper)
    ansible_extra_vars['docker_host_images'] = images
  end

  # https://www.vagrantup.com/docs/multi-machine/
  # https://wiki.debian.org/DebianBuster
  vagrant.vm.define('debian-buster') do |config|

    # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
    config.vm.box = 'debian/contrib-buster64'
    config.vm.hostname = 'buster.test'

    # https://www.vagrantup.com/docs/provisioning/ansible.html
    config.vm.provision('ansible') do |ansible|
      ansible.compatibility_mode = ansible_compatibility_mode
      ansible.extra_vars = ansible_extra_vars
      ansible.playbook = ansible_playbook
    end

  end

  # https://wiki.debian.org/DebianStretch
  vagrant.vm.define('debian-stretch') do |config|

    # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
    config.vm.box = 'debian/contrib-stretch64'
    config.vm.hostname = 'stretch.test'

    # https://www.vagrantup.com/docs/provisioning/ansible.html
    config.vm.provision('ansible') do |ansible|
      ansible.compatibility_mode = ansible_compatibility_mode
      ansible.extra_vars = ansible_extra_vars
      ansible.playbook = ansible_playbook
    end

  end

  # http://releases.ubuntu.com/18.04/
  vagrant.vm.define('ubuntu-bionic') do |config|

    # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
    config.vm.box = 'ubuntu/bionic64'
    config.vm.hostname = 'bionic.test'

    config.vm.provision('shell', inline: <<-SCRIPT)
      which python || (
        sudo apt-get -y update &&
        sudo apt-get -y install python
      )
    SCRIPT

    # https://www.vagrantup.com/docs/provisioning/ansible.html
    config.vm.provision('ansible') do |ansible|
      ansible.compatibility_mode = ansible_compatibility_mode
      ansible.extra_vars = ansible_extra_vars
      ansible.playbook = ansible_playbook
    end

  end

  # https://wiki.centos.org/Manuals/ReleaseNotes/CentOS7.1810
  vagrant.vm.define('centos-7') do |config|

    # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
    config.vm.box = 'generic/centos7'
    config.vm.hostname = 'centos7.test'

    # https://www.vagrantup.com/docs/provisioning/ansible.html
    config.vm.provision('ansible') do |ansible|
      ansible.compatibility_mode = ansible_compatibility_mode
      ansible.extra_vars = ansible_extra_vars
      ansible.playbook = ansible_playbook
    end

  end

  # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
  vagrant.vm.box_check_update = false
  vagrant.vm.post_up_message = nil

end
